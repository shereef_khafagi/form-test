
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/*
* Plugins
*/
import Vue from 'vue';
import Datepicker from 'vuejs-datepicker';
import VueTimepicker from 'vue-time-picker';
import VueMask from 'v-mask';
Vue.use(VueMask);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'

    data: {
    	name: '',
    	email: '',
    	phone: '',
    	date: '',
    	time: ''
    }

    methods: {
    	onSubmit() {
    		axios.post()
    	}
    }

    components: {
    	vuejsDatepicker,
    	VueTimepicker
    }
});
