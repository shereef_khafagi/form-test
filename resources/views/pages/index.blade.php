@extends('main')

@section('title', '| Contact' )

@section('content')

  <div id="app">
    <div class="row">
      <div class="col-md-12">
        <h1>Schedule An Appointment</h1>
        <hr>
        <form>
          <div class="form-group">
            <label name="name">Name:</label>
            <input id="name" name="name" class="form-control" v-model="name">
          </div>

          <div class="form-group">
            <label name="email">Email:</label>
            <input id="email" name="email" class="form-control" v-model="email">
          </div>

          <div class="form-group">
            <label name="phone">Phone:</label>
            <input id="phone" name="phone" class="form-control" v-model="phone" v-mask=" '###-###-###' ">
          </div>

          <div class="form-group">
            <label name="date">Date:</label>
            <input id="date" name="date" class="form-control" v-model="date">
            <datepicker></datepicker>
          </div>

          <div class="form-group">
            <label name="time">Time:</label>
            <input id="time" name="time" class="form-control" v-model="time">
            <vue-timepicker format="hh:mm:ss a"></vue-timepicker>
          </div>

          <input type="submit" value="Schedule Appointment" class="btn btn-success">
        </form>
      </div><!-- end of .col-md-12 -->
    </div><!-- end of .row -->
  </div><!-- end of #app -->
@endsection